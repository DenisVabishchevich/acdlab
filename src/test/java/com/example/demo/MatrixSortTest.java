package com.example.demo;

import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

public class MatrixSortTest {

    @Test
    public void twoStringsCompareTest() {
        Comparator comparator = new INeedToCompareLikeThis();
        boolean compare1 = comparator.compare("name1", "name2") < 0;
        boolean compare2 = comparator.compare("name3", "name2") > 0;
        boolean compare3 = comparator.compare("acd", "acd") == 0;
        Assert.assertEquals(compare1 && compare1 && compare1, true);
    }

    @Test
    public void doubleSmallerThenAnyStringFistArgDouble() {
        Comparator comparator = new INeedToCompareLikeThis();
        boolean compare = comparator.compare("2.2", "name2") < 0;
        Assert.assertEquals(compare, true);
    }

    @Test
    public void doubleSmallerThenAnyStringSecondArgDouble() {
        Comparator comparator = new INeedToCompareLikeThis();
        boolean compare = comparator.compare("name1", "2.2") > 0;
        Assert.assertEquals(compare, true);
    }

    @Test
    public void ifTwoDoubleCompareAsDoubles() {
        Comparator comparator = new INeedToCompareLikeThis();
        boolean compare1 = comparator.compare("2.1", "2.2") < 0;
        boolean compare2 = comparator.compare("2.2", "2.1") > 0;
        boolean compare3 = comparator.compare("1", "1") == 0;

        Assert.assertEquals(compare1 && compare1 && compare1, true);
    }

}
