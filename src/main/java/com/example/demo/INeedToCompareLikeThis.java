package com.example.demo;

import java.util.Comparator;

public class INeedToCompareLikeThis implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        Double first;
        Double second;
        try {
            first = Double.valueOf(o1);
            second = Double.valueOf(o2);
            return first.compareTo(second);
        } catch (NumberFormatException e) {
            try {
                Double.valueOf(o1);
                return -1;
            } catch (NumberFormatException e1) {
                try {
                    Double.valueOf(o2);
                    return +1;
                } catch (NumberFormatException e2) {
                    return o1.compareTo(o2);
                }
            }
        }
    }
}
