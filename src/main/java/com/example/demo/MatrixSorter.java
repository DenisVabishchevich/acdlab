package com.example.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MatrixSorter {

    public static List<List<String>> sort(List<String> data, Comparator<String> comparator) {
        List<List<String>> resultSortedMatrix = new ArrayList<>();
        data.forEach(elem -> resultSortedMatrix.add(Arrays.asList(elem.split("[^a-zA-Z0-9\\-\\.]+"))));  // fill matrix with data
        int deepIndex = 0;
        for (int i = 0; i < resultSortedMatrix.size() - 1; i++) {
            for (int j = i + 1; j < resultSortedMatrix.size(); j++) {
                while (true) {
                    if (resultSortedMatrix.get(i).size() > deepIndex && resultSortedMatrix.get(j).size() > deepIndex && comparator.compare(resultSortedMatrix.get(i).get(deepIndex), resultSortedMatrix.get(j).get(deepIndex)) >= 0) {
                        if (comparator.compare(resultSortedMatrix.get(i).get(deepIndex), resultSortedMatrix.get(j).get(deepIndex)) == 0) {
                            deepIndex++;
                            continue;
                        }
                        List<String> temp = resultSortedMatrix.get(i);
                        resultSortedMatrix.set(i, resultSortedMatrix.get(j));
                        resultSortedMatrix.set(j, temp);
                    }
                    break;
                }
                deepIndex = 0;
            }
        }
        return resultSortedMatrix;
    }

}
