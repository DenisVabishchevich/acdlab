package com.example.demo;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;

@SpringUI()
@Theme("valo")
public class VaadinUI extends UI {

    private static final String FILE_NAME = "file.txt";

    private Upload upload;

    private File tempFile;

    private FileOutputStream fos = null;

    private TextArea textArea;

    @Override
    protected void init(VaadinRequest request) {

        SortedFileReceiver receiver = new SortedFileReceiver();
        upload = new Upload("Файл для загрузки", receiver);
        upload.addSucceededListener(receiver);
        upload.setButtonCaption("Загрузить файл и Вывести таблицу");
        upload.setImmediateMode(false);

        textArea = new TextArea();
        textArea.setVisible(false);

        HorizontalLayout layout = new HorizontalLayout();
        layout.addComponents(upload, textArea);
        setContent(layout);
    }

    public class SortedFileReceiver implements Upload.Receiver, Upload.SucceededListener {

        @Override
        public OutputStream receiveUpload(String s, String s1) { //create temp file with loaded data
            tempFile = new File(FILE_NAME);
            try {
                return fos = new FileOutputStream(tempFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return fos;
        }

        @Override
        public void uploadSucceeded(Upload.SucceededEvent succeededEvent) {
            try {
                List<String> fileLines = Files.readAllLines(tempFile.toPath(), Charset.forName("UTF-8"));
                List<List<String>> resultMatrix = MatrixSorter.sort(fileLines, new INeedToCompareLikeThis());
                StringBuilder builder = new StringBuilder();
                for (List<String> strings : resultMatrix) {
                    builder.append(strings.toString());
                    builder.append("\n");
                }

                textArea.setWidth("500");
                textArea.setHeight("500");
                textArea.setValue(builder.toString());
                textArea.setVisible(true);

            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                try {
                    Files.delete(tempFile.toPath());  // remove temp file
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}